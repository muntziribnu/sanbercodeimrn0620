//No. 1 Looping While
console.log("No. 1 Looping While")
console.log("LOOPING PERTAMA")
var max=20
var min=2
while(min <= max){
    console.log(min+" - I love coding")
    min+=2
}
min=2
console.log("LOOPING KEDUA")
while(max >= min){
    console.log(max+" - I will become a mobile developer")
    max-=2
}
console.log("")

//No. 2 Looping menggunakan for
console.log("No. 2 Looping menggunakan for")
var awal=1;
var akhir=20;
for (awal; awal <= akhir; awal++){
    if (awal%2==0){
        var kata=" - Berkualitas"
    } else {
        var kata=" - Santai"
    }
    if (awal%3==0 && awal%2==1){
        var kata=" - I Love Coding"
    }
    console.log(awal+kata)
}
console.log("")

//No. 3 Membuat Persegi Panjang #
console.log("No. 3 Membuat Persegi Panjang #")
var n=4
var m=8
for(var i=1;i<=n;i++){
    var row=''
    for (var j=1;j<=m;j++){
        row += '#'
    }
    console.log(row)
}
console.log("")

//No. 4 Membuat Tangga
console.log("No. 4 Membuat Tangga")
var i=7
var m=1
for (i; i>=m; i--){
    var row=''
    for (var j=1;j<=(7-i+1);j++){
        row += '#'
    }
    console.log(row)
}

console.log("")

//No. 5 Membuat Papan Catur
console.log("No. 5 Membuat Papan Catur")
for (i=1;i<=8;i++){
    var row=''
    for (j=1;j<=8;j++){
        if (i%2==0){
            if(j%2!=0){
                row+='#'
            } else {
                row+=' '
            }
        }
        else{
            if(j%2!=0){
                row+=' '
            }
            else{
                row+='#'
            }
        }
    }
    console.log(row)
}