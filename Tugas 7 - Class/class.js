//1. Animal Class 
console.log("1. Animal Class")
class Animal {
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.legs = 2
    }
    yell(){
        console.log("Auooo")
    }
}
class Frog extends Animal {
    constructor(name){
        super(name)
    }
    jump(){
        console.log("hop hop")
    }
}
//Release 0
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log("")

//2. Function to Class
console.log("2. Function to Class")
class Clock {
    constructor({ template }) {
      this.template = template
    }
    render() {
      var date = new Date();
      this.hours = date.getHours();
      if (this.hours < 10) this.hours = '0' + this.hours;
  
      this.mins = date.getMinutes();
      if (this.mins < 10) this.mins = '0' + this.mins;
  
      this.secs = date.getSeconds();
      if (this.secs < 10) this.secs = '0' + this.secs;

      this.output = this.template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);

      console.log(this.output);
    }
    stop() {
      clearInterval(this.timer.bind(this));
    }
    start() {
      this.render() 
      this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  