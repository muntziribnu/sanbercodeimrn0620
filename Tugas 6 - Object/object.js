//Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    var arrObj = {}
    if (arr.length==0 || !arr){
    } else {
    for (var i=0; i<arr.length; i++){
            console.log("")
            var now = new Date()
            var thisYear = now.getFullYear() // 2020 (tahun sekarang)
            var umur = thisYear - arr[i][3]
            if (arr[i][3]==null || umur<0){
                umur = "Invalid Birth Year"
            } else {
                umur = umur
            }
            arrObj = {
                firstName : arr[i][0],
                lastName : arr[i][1],
                gender : arr[i][2],
                age : umur
            }
            console.log(++i+". "+arrObj.firstName+" "+arrObj.lastName+": {\n firstName: "+arrObj.firstName+",\n lastName: "+arrObj.lastName+",\n gender: "+arrObj.gender+",\n age: "+arrObj.age)
            i--
        }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log("")

//Soal No. 2 (Shopping Time)
console.log("Soal No. 2 (Shopping Time)")
function shoppingTime(memberId, money) {
    var barangHarga = [['Sepatu Stacattu',1500000],['Baju brand Zoro',500000],['Baju brand H&N',250000],['Sweater brand Uniklooh',175000],['Casing Handphone',50000]]
    var belanja = []
    var cash = money
    if (memberId==='' || !memberId) {
      return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if (money < 50000 && memberId != ''){
      return "Mohon maaf, uang tidak cukup"
    }
    else {
      for (var i=0; i<barangHarga.length; i++){
         if (money >= barangHarga[i][1]){
          belanja[i] = barangHarga[i][0]
          belanja.push
          money = money-barangHarga[i][1]
        }
      var Obj = {
        memberId: memberId,
        money: cash,
        listPurchased: belanja,
        changeMoney: money
    }
  }
  return Obj
  }
}
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("")

  //Soal No. 3 (Naik Angkot)
  console.log("Soal No. 3 (Naik Angkot)")
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var hasil = []
    if (arrPenumpang==[]){
      return []
    }
  else {
    for (var i=0;i<arrPenumpang.length;i++){
      var berangkat = rute.indexOf(arrPenumpang[i][1])
      var tujuan = rute.indexOf(arrPenumpang[i][2])
      var ongkos = tujuan-berangkat
      ongkos = ongkos*2000
      var penumpangOngkos = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
        bayar: ongkos
      }
      hasil.push(penumpangOngkos)
    }
    return hasil
  }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]