//Soal No. 1 (Range)
console.log("Soal No. 1 (Range)")
function range(startNum, finishNum){
    var nil = [];
    if (startNum==null || finishNum==null){
        return -1
    } else {
        if (startNum > finishNum){
            for (var i = startNum; i >= finishNum; i--){
                nil.push(i)
            }
        } else {
            for (var i = startNum; i <= finishNum; i++){
                nil.push(i)
            }
        }
        return nil;
    }   
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("")

//Soal No. 2 (Range with Step)
console.log("Soal No. 2 (Range with Step)")
function rangeWithStep(startNum, finishNum, step){
    var nil = []
    if (startNum > finishNum){
        for (var i = startNum; i >= finishNum; i-=step){
            nil.push(i)
        }
    } else {
        for (var i = startNum; i <= finishNum; i+=step){
            nil.push(i)
        }
    }
    return nil;
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("")

//Soal No. 3 (Sum of Range)
console.log("Soal No. 3 (Sum of Range)")
function sum(startNum, finishNum, step) {
    var nil = []
    var total = 0;
    if (startNum != null && finishNum == null && step == null) {
        total = startNum;
    }
    else if (startNum == null && finishNum == null && step == null) {
    } else if (step == null) {
        if (startNum > finishNum) {
            for (var i = startNum; i >= finishNum; i--) {
                nil.push(i);
            }
        } else {
            for (var i = startNum; i <= finishNum; i++) {
                nil.push(i)
            }
        }
    }
    else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            nil.push(i)
        }
    } else {
        for (var i = startNum; i <= finishNum; i += step) {
            nil.push(i)
        }
    }
    for (var index = 0; index < nil.length; index++) {
        total += nil[index];
    }
    return total;
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("")

//Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)")
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(){
    for (var i = 0; i < input.length; i++){
        console.log("Nomor ID: "+input[i][0])
        console.log("Nama Lengkap: "+input[i][1])
        console.log("TTL: "+input[i][2]+" "+input[i][3])
        console.log("Hobi: "+input[i][4])
        console.log("")
    }
    return ''
}
console.log(dataHandling())
/*
Nomor ID:  0001
Nama Lengkap:  Roman Alamsyah
TTL:  Bandar Lampung 21/05/1989
Hobi:  Membaca
 
Nomor ID:  0002
Nama Lengkap:  Dika Sembiring
TTL:  Medan 10/10/1992
Hobi:  Bermain Gitar
 
Nomor ID:  0003
Nama Lengkap:  Winona
TTL:  Ambon 25/12/1965
Hobi:  Memasak
 
Nomor ID:  0004
Nama Lengkap:  Bintang Senjaya
TTL:  Martapura 6/4/1970
Hobi:  Berkebun 
*/

//Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)")
function balikKata(str){
    var StringBaru = "";
    for (var i = str.length - 1; i >= 0; i--) {
    var StringBaru = StringBaru + str[i];
    }
    return StringBaru;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("")

//Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)")

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(){

    console.log("keluaran yang diharapkan (pada console)\n")

    input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input)

    var tanggal = input[3]
    var pisah = tanggal.split("/")
    switch(pisah[1]) {
        case '01': {console.log("Januari"); break;}
        case '02': {console.log("Februari"); break;}
        case '03': {console.log("Maret"); break;}
        case '04': {console.log("April"); break;}
        case '05': {console.log("Mei"); break;}
        case '06': {console.log("Juni"); break;}
        case '07': {console.log("Juli"); break;}
        case '08': {console.log("Agustus"); break;}
        case '09': {console.log("September"); break;}
        case '10': {console.log("Oktober"); break;}
        case '11': {console.log("November"); break;}
        case '12': {console.log("Desember"); break;}
        default: {console.log("Nggak ada");}
    }

    var urut = pisah.sort(function(a, c){return c-a});
    console.log(urut)

    var pisah2 = tanggal.split("/")
    var gabung = pisah2.join("-")
    console.log(gabung)

    var el2 = input.join("")
    var ambil = el2.slice(4, 18)
    console.log(ambil)
}

dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 