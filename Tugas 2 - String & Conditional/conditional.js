//Soal No. 1 If-else
console.log("Soal No. 1 Werewolf Game")
var nama = "John"
var peran = ""

if (nama != ""){
    if (peran == ""){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
    }
    else if(peran == "Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    }
    else if(peran == "Guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
    else if(peran == "Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!")
    }
}
else{
    console.log("Nama harus diisi!")
}
console.log("")

//Soal No. 2 Switch-case
console.log("Soal No. 2 Format Tanggal")
var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
if (tanggal > 0 && tanggal < 32){
    if (bulan > 0 && bulan < 13 ){
        if (tahun > 1899 && tahun < 2201){
            switch(bulan){
                case 1: { console.log(tanggal+" Januari "+tahun); break;}
                case 2: { console.log(tanggal+" Februari "+tahun); break;}
                case 3: { console.log(tanggal+" Maret "+tahun); break;}
                case 4: { console.log(tanggal+" April "+tahun); break;}
                case 5: { console.log(tanggal+" Mei "+tahun); break;}
                case 6: { console.log(tanggal+" Juni "+tahun); break;}
                case 7: { console.log(tanggal+" Juli "+tahun); break;}
                case 8: { console.log(tanggal+" Agustus "+tahun); break;}
                case 9: { console.log(tanggal+" September "+tahun); break;}
                case 10: { console.log(tanggal+" Oktober "+tahun); break;}
                case 11: { console.log(tanggal+" November "+tahun); break;}
                case 12: { console.log(tanggal+" Desember "+tahun); break;}
                default: { console.log("Bulan hanya dapat diisi angka 1-12")}
            }
        }
        else {
            console.log("Tahun hanya dari 1900-2200")
        }
    }
    else {
        console.log("Bulan hanya dari 1-12")
    }
}
else {
    console.log("Tanggal hanya dari 1-31")
}

